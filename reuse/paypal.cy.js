class paypal{
    static payment(){
        cy.get('#email').clear().type(' asha786@yopmail.com')
        cy.get('#btnNext').click()
        cy.get('#password').type('Admin@786')
        cy.get('#btnLogin').click()
        cy.viewport(1280, 720)
        cy.wait(30000)
      
    }
    static Login(username,password){
        cy.get('#language-switcher-locales').select("English (United States)")
        cy.get('#language-switcher > .button').click()
        cy.wait(2000)
        cy.get('#user_login').clear().type(username)
        cy.get('#user_pass').type(password)
        cy.get('#wp-submit').click({force:true})
        cy.wait(2000)
        
    }
    static Home(){
        cy.get('#wp-admin-bar-site-name > [aria-haspopup="true"]').click()
        cy.wait(2000)
        cy.get('#menu-item-542742 > .nav-top-link').contains('CO.CO WORLD')
    }
    static Addtocart(id){
        cy.contains('NEW IN').invoke('show').click()
        cy.contains("ACTIVE WEAR").click({force: true})
        cy.get(`.post-${id} > .col-inner > .product-small > .box-image > .image-fade_in_back > a > .container-image-and-badge > .attachment-woocommerce_thumbnail`)
        .scrollIntoView().click()
        cy.get('[data-value="xl"] > .yith_wccl_value').click()
        cy.get('.single_add_to_cart_button').click()
    } 
    static checkout(){
        cy.get('.woocommerce-mini-cart__buttons > [href="https://staging-cocoil.kinsta.cloud/cart/"]').click({force:true})
        cy.get('.checkout-button').click()
        cy.get('#billing_first_name').clear().type('asha')
        cy.get('#billing_last_name').clear().type('test')
        cy.get('#billing_company').clear().type('cocoil')
        cy.xpath('/html/body/div[2]/main/div[2]/div/form/div/div[2]/div/div/div[1]/div/div/div/p/label/input')
        .click()    
 }
    static Directpay(){
        cy.get('.payment_method_bacs').click({multiple:true})
        cy.wait(6000)
        cy.get('#place_order').click()
        cy.wait(3000)
        cy.contains("תודה לך. ההזמנה התקבלה בהצלחה.")
    }
    static payusingcard(){
      cy.xpath("//input[@value='zcredit_checkout_payment']")
        .click({force:true})
        cy.get('#place_order').click()
        cy.wait(3000)
        cy.get('#tbCardNumberField2').type('4580881900053219')
        cy.get('#ExpDate').type('0926')
        cy.get('#Cvv').type('730')
        cy.get('#CustomerHolderId').type('122222227')
        cy.get('.col-md-12 > .btn').click()
        //cy.get(':nth-child(12) > .col-md-12 > .btn > [ng-if="!vm.IsTransactionSuccess && !vm.IsWhileSubmit"]').click()
    }
    static myaddress(){
        cy.contains('הכתובת שלי').click({force:true})
        cy.contains('הכתובת שלי').click({force:true})
        cy.get('.u-column2 > .woocommerce-Address-title > .edit').click()
        cy.wait(4000)
        cy.get('#shipping_first_name').clear().type('asha')
        cy.get('#shipping_last_name').clear().type('lata')
        cy.get('#shipping_company').clear().type('CO.CO')
        cy.get('#shipping_address_1').clear().type('1st stage and 3rd cross')
        cy.get('#shipping_address_2').clear().type('Ramachandrapuram')
        cy.get('#shipping_city').clear().type('hyderabad')
        cy.get('#shipping_postcode').clear().type('502032')
        cy.get('.woocommerce-address-fields > :nth-child(2) > .button').click()
        cy.wait(4000)
        cy.get('.u-column1 > .woocommerce-Address-title > .edit').click()
        cy.wait(4000)
        cy.get('#billing_first_name').clear().type('asha')
        cy.get('#billing_last_name').clear().type('lata')
        cy.get('#billing_company').clear().type('CO.CO')
        cy.get('#billing_address_1').clear().type('1st stage and 3rd cross')
        cy.get('#billing_address_2').clear().type('Ramachandrapuram')
        cy.get('#billing_city').clear().type('hyderabad')
        cy.get('#billing_postcode').clear().type('502032')
        cy.get('#billing_phone').clear().type('9090909090')
        cy.get('#billing_email').clear().type('ashanewmails@gmail.com')
        cy.get('.woocommerce-address-fields > :nth-child(2) > .button').click()
    }
}
export default paypal;
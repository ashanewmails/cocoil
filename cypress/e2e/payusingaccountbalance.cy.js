/// <reference types="cypress" />

import paypal from "../../reuse/paypal.cy"
describe("Direct pay",()=>{
    it('Pay directly for the product',()=>{
        cy.visit('/')
        paypal.Login('ashanewmails','Password@123')
        paypal.Home();
        cy.get('#menu-item-545758 > .nav-top-link').click()
        const id = 574898 
        cy.get(`.post-${id} > .col-inner > .product-small > .box-image > .image-fade_in_back > a > .container-image-and-badge > .attachment-woocommerce_thumbnail`)
        .scrollIntoView().click({force:true})
        cy.get('[data-value="xl"] > .yith_wccl_value').click()
        cy.get('.single_add_to_cart_button').click()
        paypal.checkout()
        cy.xpath('/html/body/div[2]/main/div[2]/div/form[2]/div/div[2]/div/div/div[1]/div/ul/li[2]/input').click({force:true})
        cy.wait(6000)
        cy.get('#place_order').click()
        cy.wait(3000)
        cy.contains("תודה לך. ההזמנה התקבלה בהצלחה.")
            
        })
    })

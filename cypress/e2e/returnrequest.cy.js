/// <reference types="cypress" />

import paypal from "../../reuse/paypal.cy"
describe("Return request",()=>{
    it('Return request for the product',()=>{
        cy.visit('/')
        paypal.Login('ashanewmails','Password@123')
        paypal.Home();
        cy.get('#menu-item-545758 > .nav-top-link').click()
        const id = 574244 
        cy.get(`.post-${id} > .col-inner > .product-small > .box-image > .image-fade_in_back > a > .container-image-and-badge > .attachment-woocommerce_thumbnail`)
        .scrollIntoView().click({force:true})
        cy.get('[data-value="m"] > .yith_wccl_value').click()
        cy.get('.single_add_to_cart_button').click()
        paypal.checkout()
        paypal.Directpay()
        cy.get('.account-link > .icon-user').click()
        cy.get('.yith-orders > .item-label').click()
        cy.get(':nth-child(1) > .woocommerce-orders-table__cell-order-number > a').click()
        cy.get('.ywcars_button_refund_container > .button').click({force:true})
        cy.get('.yith-refund-requests > .item-label').click()
        cy.get('[data-title="Actions"] > .button').click()
        cy.get('#ywcars_new_message').type('Returning this item for testing purpose')
        cy.get('#ywcars_send_message').click()

        })
    })


/// <reference types="cypress" />

import paypal from "../../reuse/paypal.cy"
describe("Direct pay with coupen",()=>{
    it('Pay directly for the product with coupen',()=>{
        cy.visit('/')
        paypal.Login('ashanewmails','Password@123')
        paypal.Home();
        cy.get('#menu-item-545758 > .nav-top-link').click()
        const id = 574893
        cy.get(`.post-${id} > .col-inner > .product-small > .box-image > .image-fade_in_back > a > .container-image-and-badge > .attachment-woocommerce_thumbnail`)
        .scrollIntoView().click()
        cy.get('[data-value="xl"] > .yith_wccl_value').click()
        cy.get('.single_add_to_cart_button').click()
        paypal.checkout()
        cy.get('.showcoupon').click()
        cy.get('#coupon_code').type("qa-coupon-test")
        cy.get('.coupon > .flex-row > :nth-child(2) > .button').click()
        cy.get('#menu-item-545758 > .nav-top-link').click()
        const nid = 575290 
        cy.get(`.post-${nid} > .col-inner > .product-small > .box-image > .image-fade_in_back > a > .container-image-and-badge > .attachment-woocommerce_thumbnail`)
        .scrollIntoView().click()
        cy.get('[data-value="xl"] > .yith_wccl_value').click()
        cy.get('.single_add_to_cart_button').click()
        paypal.checkout()
        paypal.Directpay()        
        })
    })
    

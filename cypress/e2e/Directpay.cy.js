/// <reference types="cypress" />

import paypal from "../../reuse/paypal.cy"
describe("Direct pay",()=>{
    it('Pay directly for the product',()=>{
        cy.visit('/')
        paypal.Login('ashanewmails','Password@123')
        paypal.Home();
        cy.get('#menu-item-545758 > .nav-top-link').click()
        const id = 574898 
        cy.get(`.post-${id} > .col-inner > .product-small > .box-image > .image-fade_in_back > a > .container-image-and-badge > .attachment-woocommerce_thumbnail`)
        .scrollIntoView().click({force:true})
        cy.get('[data-value="l"] > .yith_wccl_value').click()
        cy.get('.single_add_to_cart_button').click()
        paypal.checkout()
        paypal.Directpay()        
        })
    })

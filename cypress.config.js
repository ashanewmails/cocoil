const { defineConfig } = require("cypress");
/// <reference types="Cypress" />

  
module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://staging-cocoil.kinsta.cloud/wp-login.php',
    experimentalSessionAndOrigin: true,
    chromeWebSecurity: false,
    failOnStatusCode:false,
    watchForFileChanges: false,
    defaultCommandTimeout: 60000,
    execTimeout: 60000,
    uncaught:"exception",
    pageLoadTimeout: 75000,
    requestTimeout: 60000,
    responseTimeout: 60000,
    video: true,
    retries:0,
  }
});
